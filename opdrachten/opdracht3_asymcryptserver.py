#Gebruik RSA voor Key exchange en AES voor encryption
import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

server_address = ('192.168.126.129',5000)
print('Starting up on %s port %s' % server_address)
sock.bind(server_address)

sock.listen(1)

while True:
	print('waiting for connection')
	connection, client_address = sock.accept()
	try:
        	print('Connecting from', client_address)
		x = connection.recv(1024)
		print(x)
		key=RSA.importKey(x)
            	print('Recieved Public Key')
		print('Encrypting message')
		cipher=PKCS1_OAEP.new(key)
		message=b'abcdefg'
		ciphertext= cipher.encrypt(message)
		#print(ciphertext)
               	print('Message Encrypted')
		connection.send(ciphertext)
		print('Message Sent')
	finally:
        	connection.close()

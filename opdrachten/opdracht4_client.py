# Gebruik RSA voor key exchange
import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Cipher import AES
import os
import time

def do_decrypt(ciphertext,aeskey,ivkey):
    obj2 = AES.new(aeskey, AES.MODE_CFB, ivkey)
    message1 = obj2.decrypt(ciphertext)
    return message1

key=RSA.generate(2048)
cipher=PKCS1_OAEP.new(key)
#aes_key=os.urandom(blocksize)
#iv_key=os.urandom(blocksize)
#aes_key='This is a key123'
#iv_key='This is an IV456'
#aes_key='1234567887654321'
#iv_key='qwertyuiiuytrewq'

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('192.168.126.129',5000)
print('Connecting %s port %s' % server_address)
sock.connect(server_address)

public_key=key.publickey().exportKey()
print('Sending RSA Public Key "%s"'% public_key)
sock.send(public_key)

print('Data Incoming')

time.sleep(1)
aes_key = sock.recv(1024)
aes_key = cipher.decrypt(aes_key)
print('Recieved AES Key')

time.sleep(1)
iv_key = sock.recv(1024)
iv_key = cipher.decrypt(iv_key)
print('Recieved IV Key')

time.sleep(1)
data=sock.recv(1024)
print('Recieved Data')
print('Decrypting Data')
#cipher=PKCS1_OAEP.new(key)
#message=cipher.decrypt(data)
message=do_decrypt(data,aes_key,iv_key)
print('Data Decrypted')
print('Message: "%s"' % message)

sock.close();

import socket
from Crypto.Cipher import AES

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

server_address = ('192.168.126.129',5000)
print('Starting up on %s port %s' % server_address)
sock.bind(server_address)

sock.listen(1)

def do_decrypt(ciphertext):
    obj2 = AES.new('This is a key123', AES.MODE_CBC, 'This is an IV456')
    message = obj2.decrypt(ciphertext)
    return message

while True:
    print('waiting for connection')
    connection, client_address = sock.accept()

    try:
        print('Connecting from', client_address)
        data = connection.recv(1024)
        data = data.decode('UTF-8')
        print('recieved')
        print('Decrypting')
        de_message = do_decrypt(data,)
        print('Decrypted Message: '% de_message)
    finally:
        connection.close()

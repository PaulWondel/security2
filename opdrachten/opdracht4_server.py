#Gebruik RSA voor Key exchange en AES voor encryption
import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Cipher import AES
import time




def do_encrypt(message,aeskey,ivkey):
    obj = AES.new(aeskey, AES.MODE_CFB, ivkey)
    ciphertext1 = obj.encrypt(message)
    return ciphertext1

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

server_address = ('192.168.126.129',5000)
print('Starting up on %s port %s' % server_address)
sock.bind(server_address)

sock.listen(1)

while True:
	print('waiting for connection')
	connection, client_address = sock.accept()
	
	
	try:
        	print('Connecting from', client_address)
		key = connection.recv(1024)
		key=RSA.importKey(key)
		cipher=PKCS1_OAEP.new(key)
		print(key)
		
		time.sleep(1)
		aes_key='1234567887654321'
		aes_enkey = cipher.encrypt(aes_key)
		print('Sending RSA AES Key')
		connection.send(aes_enkey)		
		
		time.sleep(1)
		iv_key='qwertyuiiuytrewq'
		iv_enkey = cipher.encrypt(iv_key)
		print('Sending RSA IV Key')
		connection.send(iv_enkey)
		
		time.sleep(1)
		print('Recieved Encryption keys')
		#key=RSA.importKey(key)
            	#print('Recieved Encryption keys')
		print('Encrypting message')
		#cipher=PKCS1_OAEP.new(key)
		message=b'abcdefg'
		ciphertext=do_encrypt(message,aes_key,iv_key)
		#ciphertext= cipher.encrypt(message)
               	print('Message Encrypted')
		connection.send(ciphertext)
		print('Message Sent')
	finally:
        	connection.close()

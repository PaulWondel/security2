import socket
import gnupg,base64
from Crypto.Cipher import AES

key = 'allow'

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('192.168.126.129',5000)
print('Connecting %s port %s' % server_address)
sock.connect(server_address)

gpg = gnupg.GPG()
def encrypt(passphrase, message):
   cipher = gpg.encrypt(message, recipients=None, symmetric='AE256', passphrase=passphrase, armor=True)
   return base64.b64encode( str(cipher) )

def do_encrypt(message):
    obj = AES.new('This is a key123', AES.MODE_CBC, 'This is an IV456')
    ciphertext = obj.encrypt(message)
    return ciphertext

mail = b'This message is encrypted at VM2'
message = do_encrypt(mail)
print('Message is Encrypted and sending "%s"'% mail)
sock.sendall(message)
sock.close();

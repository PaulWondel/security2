import socket
import gnupg,base64
from Crypto.Cipher import AES

key = 'allow'

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

server_address = ('192.168.126.129',5000)
print('Starting up on %s port %s' % server_address)
sock.bind(server_address)

sock.listen(1)

gpg = gnupg.GPG()	
def decrypt(cipher, passphrase):
   deciphered = str( gpg.decrypt( base64.b64decode(cipher), passphrase ) )
   return deciphered if deciphered is True else 'Incorrect passphrase'

def do_decrypt(ciphertext):
    obj2 = AES.new('This is a key123', AES.MODE_CBC, 'This is an IV456')
    message = obj2.decrypt(ciphertext)
    return message

while True:
	print('waiting for connection')
	connection, client_address = sock.accept()
	try:
        	print('Connecting from', client_address)
#        	while True:
		data = connection.recv(1024)
            	print('recieved')
               	print('Decrypting')
            	de_message = do_decrypt(data)
               	print('Decrypted Message: "%s"'% de_message)
	finally:
        	connection.close()

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

server_address = ('192.168.126.129',5000)
print('Starting up on %s port %s' % server_address)
sock.bind(server_address)

sock.listen(1)
while True:
    print('waiting for connection')
    connection, client_address = sock.accept()

    try:
        print('Connecting from', client_address)
        while True:
            data = connection.recv(1024)
            print('recieved "%s"' % data)
            if data:
		print('Sending data to client "%s"' % data)
		connection.sendall(data)
            else:
                print('no more data from', client_address)
    finally:
        connection.close()

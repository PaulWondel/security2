# Gebruik RSA voor key exchange
import socket
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

key=RSA.generate(2048)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('192.168.126.129',5000)
print('Connecting %s port %s' % server_address)
sock.connect(server_address)

public_key=key.publickey().exportKey()
print('Sending RSA Public Key "%s"'% public_key)
sock.send(public_key)

print('Data Incoming')

data=sock.recv(1024)
print('Recieved Data')
print('Decrypting Data')
cipher=PKCS1_OAEP.new(key)
message=cipher.decrypt(data)
print('Data Decrypted')
print('Message: "%s"' % message)

sock.close();
